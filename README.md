# Quantum Metric Automation Engineer Challenge

### Welcome to the Quantum Metric QA Automation Challenge.

The purpose of this challenge is for us to get to know your skills and process through a life coding session.
For many of these challenges, there is no right or wrong way of solving the task at hand, as there are _**many**_ different solutions.

We are requiring you to use _**Javascript**_ and our test framework, _**TestCafe**_, to complete this challenge.
You can find any and all information about it here:

- *https://devexpress.github.io/testcafe/*

#### Part I (3 self-written tests )

_*Test 1:*_

    - Login to the App
    - Validate the Q Logo is visible on the top left of the page.

_Okta User for Login:_

_Password for Login:_

_URL for Login Screen: https:qa1.quantummetric.com_

_*Test 2+3:*_

    - Navigate to the URL below and validate the existance of the _Play_ button.
    - Using the play button, validate that the replay will play.

    https://qa1.quantummetric.com/#/replay/5000241222

    _Two hints for the third test: _
    - You are free to investigate the page in a separate browser window from the TestCafe debug browser.
    - There are many different ways you can solve the 3rd test,
    but we recommend paying attention to the elements changing when a replay is playing
    and how we can use those for the validation.
